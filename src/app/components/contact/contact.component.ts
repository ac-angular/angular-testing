import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contact: any;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.contact = this.formBuilder.group({
      fullname: ['', Validators.required],            
      email: ['', [Validators.required, Validators.email]],
      issue: ['', Validators.required],
      message: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  
  onSubmit() {
    this.submitted = true;
    if (this.contact.invalid) {
        return;
    }
    return 'Mensaje Enviado !';
  }

}
