import { browser, by, element } from 'protractor';

export class HomePage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getTitleText(): Promise<string> {
    return element(by.id('first')).getText();
  }
}
