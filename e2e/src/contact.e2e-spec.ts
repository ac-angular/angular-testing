import { ContactPage } from './contact.po';
import { browser, logging } from 'protractor';

describe('Contact', () => {
  let page: ContactPage;

  beforeEach(() => {
    page = new ContactPage();
  });

  it('should display label Name', async () => {
    await page.navigateTo();
    expect(await page.getName()).toEqual('Nombre y Apellido');
  });

  it('should display value name', async () => {
    expect(await page.typeName()).toEqual('Arnaldo Ceballos');
  });

  it('should display value email', async () => {
    expect(await page.typeEmail()).toEqual('arnaldoceballos@gmail.com');
  });

  it('option Contratacion in select', async () => {
    expect(await page.click()).toEqual('Motivo');
  });

  it('should display value message', async () => {
    expect(await page.typeMessage()).toEqual('Este mensaje se genera mediante pruebas automáticas');
  });

  it('submit', async () => {
    expect(await page.submit()).toEqual('Enviar');
    await page.navigateToAbout();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
