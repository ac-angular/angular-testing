import { browser, by, element } from 'protractor';

export class ContactPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + '/contact');
  }

  async getName(): Promise<string> {
    return element(by.name('name')).getText();
  }

  async typeName(): Promise<string> {
    let name = element(by.id('name'));
    name.sendKeys('Arnaldo Ceballos');
    return name.getAttribute('value');
  }

  async typeEmail(): Promise<string> {
    let email = element(by.id('email'));
    email.sendKeys('arnaldoceballos@gmail.com');
    return email.getAttribute('value');
  }

  async click(): Promise<string>  {
    let el = element(by.css('form-select'));
    el.click();
    element(by.css('option:nth-child(1)')).click();
    return element(by.css('option:nth-child(1)')).getText();
  }

  async typeMessage(): Promise<string> {
    let message = element(by.id('message'));
    message.sendKeys('Este mensaje se genera mediante pruebas automáticas');
    return message.getAttribute('value');
  }

  async submit() {
    let el = element(by.buttonText('Enviar'));
    el.click();
    return element(by.buttonText('Enviar')).getText();
  }

  async navigateToAbout(): Promise<unknown> {
    return browser.get(browser.baseUrl + '/about');
  }
}
